# Popup module  

Popup module [demo](http://popup.module.frontend.production.adwatch.ru)

## Install  


```sh
$ npm install --save @adwatch/popup
```

## Usage  

```js
import Popup from '@adwatch/popup';		// for es6

var Popup = require('@adwatch/popup/build');	// for es5


let popup = new Popup(options);
```

#### Include styles 

```scss
//_popup.scss

@import '../../node_modules/@adwatch/popup/src/popup';
```

## Get started

#### Custom HTML layout
```html

<div class="popup popup_test">
	<div class="popup__overlay"></div>
	<div class="popup__inner">
		<div class="popup__layout">
			<div class="popup__close"></div>
			<div class="popup__content">
				Popup content
			</div>
		</div>
	</div>
</div>
```
##### Control popup with data attribute
```js
let popup = new Popup();
```

```html
<!-- open popup -->
<button data-popup-open=".popup_test">Open popup</button>

<!-- close popup -->
<button data-popup-close=".popup_test">Close popup</button>

<!-- remove popup -->
<button data-popup-remove=".popup_test">Remove popup</button>
```

##### Opening popup with js
```js
let popup = new Popup();


//jQuery

$(function() {
	// open popup
	$('button').click(function() {
		popup.open('.popup_test');
	});
	
	// close popup
	$('button').click(function() {
		popup.close('.popup_test');
	});
	
	// remove popup
	$('button').click(function() {
		popup.remove('.popup_test');
	});
	
	// create popup
	$('button').click(function() {
		popup.create('.popup_newtest', '<div>Some text in new createed popup</div>');
	});
});
```


## API

#### Options	


| Name                       |   Type     |  Description |
| :------------------------- | :--------- | :------ |
| `fixedOnIos 			   ` | `string `  | Fix for ios. Set it `true` if viewport scale == 1. Default - `false` |
| `bodyClass 			   ` | `string `  | Adding class on body when popup open. Default - `false` |
| `closePopupOnOverlay     ` | `boolean ` | closing the popup when clicking outside. Default - `false` |
| `closePopupOnEsc	       ` | `boolean ` | closing the popup when escape button keyup. Default - `false` |
| `autoCloseOtherPopups    ` | `boolean ` | closing all other popups when current popup open. Default - `true` |
| `dataPopupOpen    `        | `string `  | data attribute for popup open. Default - `data-popup-open` |
| `dataPopupClose    `       | `string `  | data attribute for popup close. Default - `data-popup-close` |
| `dataPopupRemove    `      | `string `  | data attribute for popup remove. Default - `data-popup-remove` |
| `template                ` | `string`   | popup layout template*. |
| `className               ` | `object`   | popup class names** |
| `onPopupOpen             ` | `function` | popups open callback |
| `onPopupClose            ` | `function` | popups close callback |
| `onPopupRemove           ` | `function` | popups remove callback |


\* Default template:
```html
<div class="{overlay}"></div>
<div class="{inner}">
	<div class="{layout}">
		<div class="{close}"></div>
		<div class="{content}"></div>
	</div>
</div>
```

\** Default className:

```js
{
	wrap: 'popup',
	inner: 'popup__inner',
	layout: 'popup__layout',
	close: 'popup__close',
	content: 'popup__content',
	overlay: 'popup__overlay'
}
```

### Callbacks option arguments

onPopupOpen arguments: `(current_popup)`  
onPopupClose arguments: `(current_popup)`  
onPopupRemove arguments: `(current_popup)`




## Methods	

#### popup.open(popup, html, callback)
Possible use - `popup.open(popup, callback)`

Open popup method
 
| Name       |   Type     |  Description |
|------------|:-----------|:------|
| `popup   ` | `string  ` | popup class or id, required |
| `html    ` | `string  ` | replace popup content, optional |
| `callback `| `function` | callback on popup opened, optional |


#### popup.create(popup, html, callback)
Possible use - `popup.create(popup, callback)`

Creating popup by template method
 
| Name       |   Type     |  Description |
|------------|:-----------|:------|
| `popup   ` | `string  ` | popup class or id, required |
| `html    ` | `string  ` | replace popup content, optional |
| `callback `| `function` | callback on popup created, optional |


#### popup.close(popup, callback)

Closing popup method

| Name       |   Type     |  Description |
|------------|:-----------|:------|
| `popup   ` | `string  ` | popup class or id, optional |
| `callback `| `function` | callback on popup closed, optional |

#### popup.remove(popup, callback)

Removing popup method

| Name       |   Type     |  Description |
|------------|:-----------|:------|
| `popup   ` | `string  ` | popup class or id, required |
| `callback `| `function` | callback on popup removed, optional |





## License

MIT © 
